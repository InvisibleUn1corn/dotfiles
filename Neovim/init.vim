call plug#begin('~/.config/nvim/plugged')

" This is the colorscheme I'm using
Plug 'morhetz/gruvbox' 
" A simple file browser for when I need to search for files in a directory
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
" Help to navigate between classes and functions
Plug 'majutsushi/tagbar', { 'on': 'Tagbar' }
" Easy commenting in Vim
Plug 'Tpope/vim-commentary'
" Plugin to move lines and selections up and down
Plug 'matze/vim-move'
" Fold code blocks
Plug 'tmhedberg/SimpylFold'
" Auto indent python
Plug 'vim-scripts/indentpython.vim'
" Match HTML tags
Plug 'Valloric/MatchTagAlways'
" Great autocomplete for JavaScript, Python, C/C++ and more
Plug 'Valloric/YouCompleteMe'
" Generate config files for C/C++ YouCompleteMe
Plug 'rdnetto/YCM-Generator', { 'branch': 'stable'}
" fzf is file fuzzy search
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
" Go autocomplete, linter and more
Plug 'fatih/vim-go'
" git plugins
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'

" Syntax plugins
Plug 'jwalton512/vim-blade'
Plug 'tkztmk/vim-vala'
Plug 'leafgarland/typescript-vim'
Plug 'mxw/vim-jsx'
Plug 'pangloss/vim-javascript'

" Neovim plugins
Plug 'kassio/neoterm'
Plug 'benekastah/neomake'

call plug#end()

" basic settings
set number
set encoding=utf-8
set laststatus=1

" colorscheme settings
let g:gruvbox_italic=1
set bg=dark
colorscheme gruvbox

" split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Enable folding
set foldmethod=indent
set foldlevel=99

" Python PEP8 standards
au BufNewFile,BufRead *.py
    \ setlocal tabstop=4
    \ softtabstop=4
    \ shiftwidth=4
    \ textwidth=79
    \ expandtab
    \ autoindent
    \ fileformat=unix

set tabstop=4
set shiftwidth=4
" set expandtab

" Highlight eslintrc as json
au BufReadPost .eslintrc set syntax=json
au BufReadPost .tern-project set syntax=json
autocmd! BufWritePost * Neomake

" Prevent vim from creating .swp files
set noswapfile

" Set leader key
let mapleader = "\<Space>"

" Neomake settings
let g:neomake_c_gcc_args = ['-fsyntax-only', '-Wall']
let g:neomake_javascript_enabled_makers = ['xo']

" Disable YouCompleteMe checker
let g:ycm_show_diagnostics_ui = 0
let g:ycm_autoclose_preview_window_after_insertion = 1

" Relative number toggle
function! NumberToggle()
  if(&relativenumber == 1)
    set norelativenumber
  else
    set relativenumber
  endif
endfunc

nnoremap <C-n> :call NumberToggle()<cr>

au FocusLost * :set norelativenumber
au FocusGained * :set relativenumber

" fzf settings
map <c-c> :Buffers<Enter>
map <c-z> :Files<Enter>
map <A-t> :Tags<Enter>

" Enable switching buffers without saving
set hidden

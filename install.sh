if ! [ "$(uname -s)" == "Linux" ]; then
    echo "ERR: Linux only"
    exit 1
fi

cp ./Git/.gitconfig ~/.gitconfig
echo "Copy git config"

mkdir -p ~/.config/nvim/autoload/
cp ./Neovim/init.vim ~/.config/nvim/init.vim
curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
echo "Copy nvim config"

cp ./YouCompleteMe/.ycm_extra_conf.py ~/.ycm_extra_conf.py
cp ./YouCompleteMe/.ycm_extra_conf.pyc ~/.ycm_extra_conf.pyc
echo "Copy YouCompleteMe files"

nvim -c 'PlugInstall' -c 'qa!'
echo "Installed vim plugins"
